# Getting Started Guide

> This guide assumes you're comfortable with git and your preferred C# code editor.

## To run this program, please complete the following steps:
1. Download and install the latest [DotNet Core Runtimes](https://www.microsoft.com/net/download)
2. Clone the [Finite.Commands](https://gitlab.com/FiniteReality/Finite.Commands/) repository
3. Build the Finite.Commands library
4. Clone this project
5. Add a reference and find the `Finite.Commands.Core.dll` file in your Finite Commands library
6. Run this project.

## To set up your own project which targets Finite.Commands, complete the following steps
1. Clone the [Finite.Commands](https://gitlab.com/FiniteReality/Finite.Commands/) repository
2. Build the Finite.Commands library
3. Make a new .NET Core Console app (or .NET Framework Console app)
4. Add a reference to `Finite.Commands.Core.dll`
5. Add a reference to the `Microsoft.Extensions.DependencyInjection.Abstractions` NuGET package
6. Create a [CommandContext](https://gitlab.com/Foxtrek_64/finite.commands.demo/blob/master/Finite.Commands.ConsoleDemo/Commands/CommandContext.cs)
7. Create a few [Commands](https://gitlab.com/Foxtrek_64/finite.commands.demo/blob/master/Finite.Commands.ConsoleDemo/Commands/Commands.cs)
8. Build your CommandService:
```cs
CommandService<CommandContext> _commandService = new CommandServiceBuilder<CommandContext>()
                .AddModules(Assembly.GetEntryAssembly())
                .AddCommandParser<DefaultCommandParser<CommandContext>>()
                .BuildCommandService();
```
9. Add a CommandHandler:
```cs
private async Task HandleCommand(string command)
{
	var context = new CommandContext()
	{
		Commands = _commandService,
		Author = $"{Environment.UserName}@{Environment.UserDomainName}",
		Message = command
	};

	var result = await _commandService.ExecuteAsync(context, null);
	
	// Do something with the result
}
```
