﻿using System;

namespace Finite.Commands.ConsoleDemo.Attributes
{
    public sealed class AboutAttribute : Attribute
    {
        public string Information { get; }

        public AboutAttribute(string information)
        {
            Information = information;
        }
    }
}
