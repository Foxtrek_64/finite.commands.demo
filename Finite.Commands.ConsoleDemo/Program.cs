﻿using Finite.Commands.ConsoleDemo.Commands;
using Finite.Commands.Extensions;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Finite.Commands.ConsoleDemo
{
    public class Program
    {
        private static readonly bool IsDebug = Debugger.IsAttached;

        // A cheap, easy way to kill the app...
        public static bool IsRunning = true;

        private static CommandService<CommandContext> _commandService;


        public static void Main() => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            Console.WriteLine("Finite.Commands Sample App");

            _commandService = new CommandServiceBuilder<CommandContext>()
                .AddModules(Assembly.GetEntryAssembly())
                .AddCommandParser<DefaultCommandParser<CommandContext>>()
                .BuildCommandService();

            StringBuilder sb = new StringBuilder();

            Console.WriteLine();
            Console.Write("> ");

            while (IsRunning)
            {
                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);

                    switch (key.Key)
                    {
                        case ConsoleKey.Enter:
                            // Push output to the next line (after the command line)
                            Console.WriteLine();
                            await HandleCommand(sb.ToString());
                            sb.Clear();
                            break;
                        case ConsoleKey.Backspace when sb.Length > 0:
                            Console.Write("\b \b");
                            sb.Length--;
                            break;
                        default:
                            if (((int)key.Key) >= 32 && ((int)key.Key <= 126))
                            {
                                sb.Append(key.KeyChar);
                                Console.Write(key.KeyChar);
                            }
                            break;
                    }
                }
            }
        }

        private async Task HandleCommand(string command)
        {
            void Finalize()
            {
                // Move to the next line and prepare for next command
                Console.WriteLine();
                Console.Write("> ");
            }

            if (null == command)
            {
                Finalize();
                return;
            }

            var context = new CommandContext()
            {
                Commands = _commandService,
                Author = $"{Environment.UserName}@{Environment.UserDomainName}",
                Message = command
            };

            var result = await _commandService.ExecuteAsync(context, null);

            Finalize();
        }
    }
}
