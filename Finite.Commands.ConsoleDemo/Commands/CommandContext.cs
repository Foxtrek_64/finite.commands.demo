﻿namespace Finite.Commands.ConsoleDemo.Commands
{
    public sealed class CommandContext : ICommandContext<CommandContext>
    {
        public CommandService<CommandContext> Commands { get; set; }

        public string Message { get; set; }

        public string Author { get; set; }
    }
}
