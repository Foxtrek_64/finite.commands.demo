﻿using Finite.Commands.ConsoleDemo.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finite.Commands.ConsoleDemo.Commands
{
    public class Commands : ModuleBase<CommandContext>
    {
        [Command("help")]
        [About("Shows how to use this app.")]
        public Task Help()
        {
            foreach (var command in Context.Commands.Modules.SelectMany(x => x.Commands))
            {
                List<string> parameters = new List<string>();
                foreach (var parameter in command.Parameters)
                {
                    parameters.Add($"[{parameter.Type}] {parameter.Aliases.FirstOrDefault()}");
                }

                AboutAttribute about = command.Attributes.FirstOrDefault(x => x is AboutAttribute) as AboutAttribute;

                Console.WriteLine($"Command: {command.Aliases.FirstOrDefault()}");
                Console.WriteLine($"\tAliases: {(command.Aliases.Count > 1 ? string.Join(", ", command.Aliases.Skip(1)) : "None")}");
                Console.WriteLine($"\tParameters: {(command.Parameters.Count > 0 ? string.Join(", ", parameters) : "None")}");
                Console.WriteLine($"\tAbout: {about?.Information ?? "None"}");
            }
            return Task.CompletedTask;
        }

        [Command("ping")]
        [About("Tests to see if the service is responding.")]
        public Task Ping()
        {
            Console.WriteLine("Pong!");
            return Task.CompletedTask;
        }

        [Command("say")]
        [About("Echoes any arguments that you provide.")]
        public Task Say(string text)
        {
            Console.WriteLine(text);
            return Task.CompletedTask;
        }

        [Command("whoami")]
        [About("Returns the Author from the CommandContext.")]
        public Task WhoAmI()
        {
            Console.WriteLine(Context.Author);
            return Task.CompletedTask;
        }

        [Command("exit")]
        [About("Closes the app.")]
        public async Task Exit()
        {
            Console.WriteLine("Shutting down...");
            await Task.Delay(500);
            Program.IsRunning = false;
        }
    }
}
